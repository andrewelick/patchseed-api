import Adafruit_DHT
from datetime import datetime

#Convert Celcius to Fahrenheit
def convert_temp(celcius):
    fahr = (celcius * (9/5)) +32
    return fahr

#Get avg of minute
def min_avgs(readings, new_temp, new_humi):
    if len(readings['temp']) == 30:
        avg_temp = sum(readings['temp']) / 30
        avg_humi = sum(readings['humi']) / 30
        readings = {'temp': [avg_temp], 'humi': [avg_humi]}

        return readings, avg_temp, avg_humi
    else:
        readings['temp'].append(new_temp)
        readings['humi'].append(new_humi)
        
        return readings

#Get temp and humi readings
def get_temp_reading(pin):
    sensor = Adafruit_DHT.DHT11
    readings = {
                'temp': [],
                'humi': []
            }

    count = 0
    
    while True:
        #Humidity, temperature, and current time
        humi, temp = Adafruit_DHT.read_retry(sensor, pin)
        usa_temp = convert_temp(temp)
        current_time = datetime.now().strftime('%-I:%M:%S%p')
        avgs = min_avgs(readings, usa_temp, humi)
        
        #Show reading if no remainder
        if count % 2:
            print(f'Temp: {usa_temp}F Humi: {humi}% {current_time}')
        
        #If avgs came in
        if not isinstance(avgs, dict):
            readings, avg_temp, avg_humi = avgs
            
            print("")
            print("")
            print(f'Avg temp: {avg_temp} & Avg Humi: {avg_humi}')
            print("")
            print("")

        #Add to count
        count += 1
        
get_temp_reading(16)
