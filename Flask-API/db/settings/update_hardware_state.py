from datetime import datetime
from ..connect_db import create_connection

#Update state of hardware in database
def update_hardware_state(pin:int, new_state:bool):
    print('Saving new state in database')

    if new_state:
        result = 1
    else:
        result = 0

    print(f'Pin: {pin} is now at a state of {result}')

    current_time = datetime.now()

    #Connect to database
    conn = create_connection()
    cursor = conn.cursor()

    #Update state
    cursor.execute("UPDATE settings SET state = ?, last_updated = ? WHERE pin = ?", (result, current_time, pin,))

    #Save results
    conn.commit()

    #Close connection
    conn.close()

    return True

