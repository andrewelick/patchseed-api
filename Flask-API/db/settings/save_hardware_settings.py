import json
from datetime import datetime
from ..connect_db import create_connection

#Updates or Inserts into settings table
def save_hardware_settings(pin, custom_settings, state= None, label= None):    
    current_time = datetime.now()

    #Connect to database
    conn = create_connection()
    cursor = conn.cursor()
    
    print(f'State coming in is: {state}')
    print(f'Label coming in is: {label}')
    
    if state is None:
        state = 0

    if label is None:
        label = "unknown"

    print(f'New State is: {state}')
    print(f'New Label is: {label}')
    settings = (pin, label, state, json.loads(custom_settings), current_time)

    #Change state & custom settings
    if state:
        query = "INSERT INTO settings (pin, label, state, custom_settings, last_updated) VALUES (?, ?, ?, ?, ?) ON CONFLICT(pin) DO UPDATE SET state = excluded.state, custom_settings = excluded.custom_settings, last_updated = excluded.last_updated WHERE pin = excluded.pin"
    else:
        #Change only custom settings 
        query = "INSERT INTO settings (pin, label, state, custom_settings, last_updated) VALUES (?, ?, ?, ?, ?) ON CONFLICT(pin) DO UPDATE SET label = excluded.label, custom_settings = excluded.custom_settings, last_updated = excluded.last_updated WHERE pin = excluded.pin"

    cursor.execute(query, settings)

    #Save results
    conn.commit()

    #Close connection
    conn.close()

    return {
                'pin': pin,
                'label': label,
                'state': state,
                'custom_settings': custom_settings,
                'last_updated': current_time
            }

