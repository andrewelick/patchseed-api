import json
from datetime import datetime
from ..connect_db import create_connection

#Get settings
def get_hardware_settings(pin):
    #Connect to database
    conn = create_connection()
    cursor = conn.cursor()

    cursor.execute("SELECT * FROM settings WHERE pin = ?", (pin,))
    query = list(cursor.fetchone())
    
    #If row found
    if query:
        settings = {
                    'pin': query[0],
                    'label': query[1],
                    'state': query[2],
                    'custom_settings': json.loads(query[3]),
                    'last_updated': query[4]
                }
    else:
        print(f"Warning: could not find settings for pin {pin}")
        settings = {}

    return settings

