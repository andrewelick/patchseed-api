import json
from datetime import datetime
from ..connect_db import create_connection

#Get settings
def get_light_settings(pin):
    #Connect to database
    conn = create_connection()
    cursor = conn.cursor()

    cursor.execute("SELECT * FROM settings WHERE pin = ?", (pin,))
    query = cursor.fetchone()

    #If row found
    if query:
        #Create object
        settings = {}
        settings['pin'], settings['label'], settings['state'], settings['custom_settings'], settings['last_updated'] = query
        
        #Stringify custom settings
        settings['custom_settings'] = json.loads(settings['custom_settings'])

    else:
        print(f"Warning: could not find settings for pin {pin}")
        settings = {}

    return settings

#Update timer settings
def save_light_cycle(pin, cycle_state, start_time, stop_time):
    #Format data
    custom_settings = json.dumps({
                'cycle': {
                    'state': cycle_state,
                    'start_time': start_time,
                    'stop_time': stop_time
                }
            })
    
    current_time = datetime.now()

    #Connect to database
    conn = create_connection()
    cursor = conn.cursor()

    #Create insert
    cursor.execute("INSERT INTO settings (pin, label, custom_settings, last_updated) VALUES (?, ?, ?, ?) ON CONFLICT(pin) DO UPDATE SET custom_settings = excluded.custom_settings, last_updated = excluded.last_updated WHERE pin = ?", (pin, 'Lights', custom_settings, current_time, pin,))

    #Save results
    conn.commit()

    #Close connection
    conn.close()

    return custom_settings

