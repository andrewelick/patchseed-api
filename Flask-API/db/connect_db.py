import sqlite3
from sqlite3 import Error

#Connect to database
def create_connection():
    conn = None
    db_file = 'patchseed.db'

    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
