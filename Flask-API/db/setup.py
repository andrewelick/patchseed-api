from connect_db import create_connection
import sqlite3

#Connect to database
conn = create_connection()

print("Creating settings table")

#Create settings table
conn.execute('''
            CREATE TABLE IF NOT EXISTS settings (
                pin TINYINT PRIMARY KEY NOT NULL,
                label VARCHAR(255) NOT NULL,
                state TINYINT NOT NULL DEFAULT 0,
                custom_settings TEXT,
                last_updated TEXT
            );''')

#Close connection
conn.close()
