from datetime import datetime

#Flask HTTP
from flask import Flask, Response, request, jsonify
from flask_cors import CORS

#Utils
from utils.toggle_hardware import toggle_hardware #Control hardware

#Database calls
from db.settings.update_hardware_state import update_hardware_state #Save hardware state
from db.settings.light_settings import get_light_settings, save_light_cycle
from db.settings.get_hardware_settings import get_hardware_settings
from db.settings.save_hardware_settings import save_hardware_settings

#Scheduling tasks
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.job import Job

#Initalize webserver
app = Flask(__name__)

#Allow CORS
CORS(app)

#Confiure and start scheduler
print("Configuring scheduler")
scheduler = BackgroundScheduler()
print("Running scheduler")
scheduler.start()

#Save settings for a piece of hardware
@app.route('/hardware/<int:pin>/settings', methods=["POST"])
def handle_save_hardware(pin):
    if pin is False:
        return jsonify({
                'error': f'Could not save settings for {pin}',
                'reason': 'Missing required pin parameter'
            }), 400
    
    #Convert to json, get params
    data = request.get_json()
    label = data.values.get('label')
    state = data.values.get('state')
    custom_settings = data.values.get('custom_settings')
    
    #Save to DB
    settings_saved = save_hardware_settings(pin, custom_settings, state = state, label = label)

    return jsonify(settings_saved), 201

#Get settings of a piece of hardware
@app.route('/hardware/<int:pin>/settings', methods=['GET'])
def getSettings(pin):
    if pin is False:
        return jsonify({
                'error': f'Could not get settings for {pin}',
                'reason': 'Missing required pin parameter'
            }), 400
    
    #Fetch settings for pin
    return jsonify(get_hardware_settings(pin))

#Light settings
@app.route('/tasks/lights/<int:pin>', methods=['GET'])
def handle_lights(pin):
    #Get lighting details
    return jsonify(get_light_settings(pin))

#Toggle lights
@app.route('/tasks/lights/toggle/<int:pin>')
def change_lights(pin):
    new_state = toggle_hardware(pin)

    #Update in database
    update_hardware_state(pin, new_state)
    
    return jsonify({'new_state': new_state}), 201

#Update light cycle settings [start, stop]
@app.route('/tasks/lights/cycle/<int:pin>/<string:state>', methods=['POST'])
def change_light_cycle(pin, state):
    start_time = request.args.get('start_time')
    stop_time = request.args.get('stop_time')
    
    #Validate state
    if state not in ['on', 'off']:
        return jsonify({
                'error': 'Could not update light cycle',
                'reason': 'Invalid option for state, must be either on or off'
            }), 400
    
    #Get running jobs
    running_jobs = scheduler.get_jobs()
    running_job_ids = []

    for job in running_jobs:
        running_job_ids.append(job.id)
    
    print(f'Running job ids: {running_job_ids}')

    #Light schedule is on
    if state == 'on':
        #Parse times
        start_hour, start_minute = start_time.split(':')
        stop_hour, stop_minute = stop_time.split(':')
        
        #Update or add light cycle job
        if 'start_lights' in running_job_ids:
            print("Updating start_lights job")
            scheduler.remove_job('start_lights')
        
        print('Starting start_lights job')
        scheduler.add_job(
                toggle_hardware,
                args= [pin],
                id= 'start_lights',
                trigger='cron',
                hour= start_hour,
                minute= start_minute
            )
        
        #Add stop lights job if not exists
        if 'stop_lights' in running_job_ids:
            print(f"Updating stop_lights job")
            scheduler.remove_job('stop_lights')
        
        print("Adding stop_lights job")
        scheduler.add_job(
                toggle_hardware,
                args= [pin],
                id='stop_lights',
                trigger='cron',
                hour= stop_hour,
                minute= stop_minute
            )
    else:
        #Check for running jobs and stop
        if 'start_lights' in running_job_ids:
            print('Removing start_lights job')
            scheduler.remove_job('start_lights')
        
        if 'stop_lights' in running_job_ids:
            print('Removing stop_lights job')
            scheduler.remove_job('stop_lights')
    
    all_jobs = scheduler.get_jobs()
    print(f'All running jobs: {all_jobs}')
    #Update in database
    response = save_light_cycle(pin, state, start_time, stop_time)
    
    return response, 200 


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
