import RPi.GPIO as GPIO

#Human label for logs
hardware_lookup = {
            27: 'lights',
            18: 'fan #1'
        }

#Switch state of a piece hardware by GPIO pin number
def toggle_hardware(pin):
    if pin not in hardware_lookup:
        hardware_lookup[pin] = 'unknown'
    
    print(f"Toggling {hardware_lookup[pin]} on pin {pin}")

    #Configure number system for pins
    GPIO.setmode(GPIO.BCM)
    
    print("Settings up GPIO channel")
    #Setup channel
    GPIO.setup(pin, GPIO.OUT)
    
    current_state = GPIO.input(pin)

    print(f"Current state was {current_state}")

    new_state = not current_state

    #Toggle pin status
    GPIO.output(pin, new_state)
    
    print(f"New state for {pin} is {new_state}")
    return new_state
