import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import{ DashboardPage } from './pages/dashboardPage'

function App() {
    return (
        <div>
            <Router>
                <div className="appBody">
                    <Route path="/" component={(props) => <DashboardPage {...props} />} />
                </div>
            </Router>
        </div>
    );
}

export default App;
