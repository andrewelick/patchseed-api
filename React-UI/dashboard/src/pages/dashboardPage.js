import React, { useEffect } from 'react';

//Components
import { Header } from '../components/header'
import { Lighting } from '../components/lighting'
import { Fans } from '../components/fans'
import { TempHumi } from '../components/temphumi'  

const moment = require('moment'); //Time formatting

export const DashboardPage = () => { 
    //Used for determining if a request is loading
    const [launchRequestsLoaded, updateLaunchRequests] = React.useState(false)   
    //Status of lights
    const [lightState, updateLights] = React.useState(false)

    //Status of light cycle
    const [lightCycleState, updateLightCycle] = React.useState('off')

    //Status of Fan
    const [fanState, updateFan] = React.useState(false)

    //Start and Stop times of light cycle
    const [startingTime, updateStartingTime] = React.useState()
    const [stoppingTime, updateStoppingTime] = React.useState()

    //Temperature & Humidity readings
    const [temp, updateTemp] = React.useState(0)
    const [humi, updateHumi] = React.useState(0)
    const [tempHumiReadings, updateTempHumiReadings] = React.useState([])
    const [tempHumiLastSaved, updateTempHumiLastSaved] = React.useState()

    //Universal vars
    const raspberryIP = '192.168.1.4' //IP of the raspberry pi, use for API calls
    const lightsPin = 27
    const fansPin = 17
    const tempPin = 16

    //Fetch hardware settings
    const getHardwareSettings = pin => {
        return new Promise((resolve, reject) => {
            console.log(`Making http request for settings of ${pin}`)
            const url = `http://${raspberryIP}:5000/hardware/${pin}/settings`
            
            fetch(url)
                .then(response => response.json())
                .then(settings => {
                    resolve(settings)
                    return
                })
                .catch(error => {
                    console.log(error)
                    reject(error)
                    return
                })
            })
    }

    //Handle hardware state change
    const handleHardwareChange = pin => {
        const url = `http://${raspberryIP}:5000/tasks/lights/toggle/${pin}`

        //Send toggle to API
        fetch(url)
            .then(response => response.json())
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
        
        //Pick which pin state should update
        switch (pin) {
            case 27:
                updateLights(!lightState) //Update lights
                break
            case fansPin:
                updateFan(!fanState)
                break
            default:
                console.log(`Error, could not update state on pin ${pin}`)
        }
        
    }

    //When user turns on/off light cycle
    const handleLightCycleChange = () => {
        let newCycleState = lightCycleState ? 'on' : 'off'
        const url = `http://${raspberryIP}:5000/tasks/lights/cycle/${lightsPin}/${newCycleState}`
        const body = {
            "start_time": startingTime,
            "stop_time": stoppingTime
        }

        //Send request to API to update database
        fetch(url, {
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(body)
        })
            .then(response => response.json())
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
        
        //Update light cycle state
        updateLightCycle(!lightCycleState)
    }

    //When user chooses a new start or stop time
    const handleLightCycleTimeChange = event => {
        const newTimeName = event.target.name
        const newTimeValue = event.target.value

        //Convert light cycle state for url
        const url = `http://${raspberryIP}:5000/tasks/lights/cycle/${lightsPin}/${lightCycleState}`
        const body = {}

        //Determine which time is getting updated
        if (newTimeName === 'newStartingTime') {
            body.start_time = newTimeValue
            body.stop_time = stoppingTime
            //Update starting time state
            updateStartingTime(newTimeValue)
        } else {
            body.start_time = startingTime
            body.stop_time = newTimeValue
            //Update stopping time state
            updateStoppingTime(newTimeValue)
        }
        
        //Send POST to API to update database
        fetch(url, {
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(body)
        })
            .then(response => response.json())
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
    }

    //Get Light settings
    const getLightSettings = async () => {
        console.log("Fetching fan settings")
        const lightSettings = await getHardwareSettings(lightsPin)
        const cycleSettings = lightSettings.custom_settings.cycle

        //Update light state
        lightSettings.state === 1 ? updateLights(true) : updateLights(false)
        
        updateLightCycle(cycleSettings.state) //Update light cycle state
        updateStartingTime(cycleSettings.start_time) //Cycle starting time
        updateStoppingTime(cycleSettings.stop_time) //Cycle stopping time
        
        return true
    }

    //Get Fan settings
    const getFanSettings = async () => {
        console.log("Fetching fan settings")
        const fanSettings = await getHardwareSettings(fansPin)

        //Update light state
        if (fanSettings.state === 1) {
            updateFan(true)
        } else {
            updateFan(false)
        }

        return true
    }

    //Get Temperature and humidity
    const getTempHumi = async () => {
        console.log("Fetching Temp&Humi readings")
        const tempHumiSettings = await getHardwareSettings(tempPin)
        
        console.log(tempHumiSettings)
        const { current_temp, current_humi, readings } = tempHumiSettings.custom_settings
        const { last_updated } = tempHumiSettings

        //Get results within 12hrs
        const hour12Ago = moment().subtract(12, 'hours').format()
        const tempHumiReadings12h = readings.filter(row => {
            const timeSaved = moment(row.timeSaved).format()
            return timeSaved > hour12Ago
        })

        //Get 30 minute readings from the 12hrs
        const every30min = tempHumiReadings12h.filter((row, index) => {
            return index % 5 == 0
        })

        //Add human readable timestamp key
        every30min.forEach(row => {
            const timeSaved = row.timeSaved
            const humanTime = moment(timeSaved).format('h:mmA')
            row.humanTime = humanTime
        })
            
        updateTemp(current_temp)
        updateHumi(current_humi)
        updateTempHumiReadings(every30min)
        updateTempHumiLastSaved(moment(last_updated).fromNow())

        return true
    }

    //Send fetch for light settings
    useEffect(() => {
        if (!launchRequestsLoaded) {
            getLightSettings()
            getFanSettings()
            getTempHumi()
            updateLaunchRequests(true)
        }       
    })

    return (
        <React.Fragment>
            <Header />
            <TempHumi
                temp = {temp}
                humi = {humi}
                readings = {tempHumiReadings}
                lastUpdated = {tempHumiLastSaved}
            />

            <Lighting 
                lightState={lightState} 
                handleLightChange={() => handleHardwareChange(lightsPin)} 
                lightCycleState={lightCycleState} 
                handleLightCycleChange={handleLightCycleChange} 
                startingTime={startingTime} 
                stoppingTime={stoppingTime} 
                handleLightCycleTimeChange = {handleLightCycleTimeChange}
            />

            <Fans 
                handleFanChange= {() => handleHardwareChange(fansPin)}
            />
        </React.Fragment>
    )
}