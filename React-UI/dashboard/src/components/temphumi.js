import React from 'react'
import './css/lighting.css'

import { ResponsiveContainer, AreaChart, Area, XAxis, YAxis } from 'recharts'
import { max } from 'moment'

export const TempHumi = (props) => {
    //Light settings props
    const { temp, humi, readings, lastUpdated } = props

    return (
        <div className="lightingContainer">
            <h1 className="titleText">
                <img src='./climate.png' alt='climate' className='iconImg' />
                Climate
            </h1>
            
            <div className="tempColorChange">
                <div className="chartContainer">
                    <div className="readingLabel">Temperature</div> 
                    <span>{temp}<span className="smallerSymbol">F</span></span>
                    <ResponsiveContainer height={100} width="100%">
                        <AreaChart  
                            height={80} 
                            data={readings}
                            margin= {{ top: 10, right: 0, bottom: 0, left: -30 }}
                        >
                            <YAxis
                                domain={['dataMin', 'dataMax']}
                                interval='preserveStart'
                                axisLine={false}
                                tickLine={false}
                                scale='linear'
                                margin={{ top: 0, right: 0, bottom: 0, left: 0 }}
                                style={{ fontSize: '11px' }}
                            />

                            <XAxis
                                dataKey="humanTime"
                                axisLine={false}
                                tickLine={false}
                                interval="preserveStartEnd"
                                minTickGap={30}
                                style={{ fontSize: '11px', fontweight: 'bold' }}
                            />

                            <defs>
                                <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                                    <stop offset="50%" stopColor="#FF416C" stopOpacity={.6}/>
                                    <stop offset="80%" stopColor="#667db6" stopOpacity={.6}/>
                                </linearGradient>
                            </defs>
                            
                            <Area type="monotone" dataKey="temp" stroke="url(#colorPv)" strokeWidth={3} fillOpacity={0} dot={false} />
                        </AreaChart>
                    </ResponsiveContainer>
                </div>

                <div className="chartContainer">
                    <div className="readingLabel">Humidity</div>  
                    <span>{humi}<span className="smallerSymbol">%</span></span>

                    <ResponsiveContainer height={100} width="100%">
                        <AreaChart  
                            data={readings}
                            margin= {{ top: 10, right: 0, bottom: 0, left: -30 }}
                        >
                            <YAxis
                                domain={['dataMin - 5', 'dataMax + 5']}
                                interval='preserveStart'
                                axisLine={false}
                                tickLine={false}
                                scale='linear'
                                padding={{ top: 0, right: 0, bottom: 0, left: 0 }}
                                style={{ fontSize: '11px', fontWeight: 'bold' }}
                            />
                            
                            <XAxis
                                dataKey="humanTime"
                                axisLine={false}
                                tickLine={false}
                                interval="preserveStartEnd"
                                minTickGap={30}
                                style={{ fontSize: '11px', fontweight: 'bold' }}
                            />
                            <defs>
                                <linearGradient id="humiColor" x1="0" y1="0" x2="0" y2="1">
                                    <stop offset="10%" stopColor="#667db6" stopOpacity={0.8}/>
                                    <stop offset="50%" stopColor="#667db6" stopOpacity={.3}/>
                                </linearGradient>
                            </defs>
                            
                            <Area type="monotone" dataKey="humi" stroke="url(#humiColor)" strokeWidth={3} fillOpacity={0} dot={false} />
                        </AreaChart>
                    </ResponsiveContainer>
                </div>
            </div> 
            <span className="timeUpdated">Updated {lastUpdated}</span>     
        </div>
    )
}