import React from 'react'
import './css/lighting.css'
import Switch from '@material-ui/core/Switch'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

export const Lighting = (props) => {
    //Light settings props
    const { lightState, handleLightChange, lightCycleState, handleLightCycleChange, startingTime, stoppingTime, handleLightCycleTimeChange } = props

    return (
        <div className="lightingContainer">
            <h1 className="titleText">
                <img src='./light-bulb.png' alt='light bulb' className='iconImg' />
                Lights
            </h1>            
            
            <div className="lightSetting">
                <span>Lights</span>
                <Switch 
                    checked={lightState} 
                    value="Lightboi"
                    onChange={handleLightChange}
                />
            </div>
                
            <div className="lightSetting">
                <span>Light Cycle</span>
                <Switch 
                    checked={lightCycleState} 
                    value="Lightboi"
                    onChange={handleLightCycleChange}
                />

                {
                    lightCycleState ?
                    <React.Fragment>
                        <div className="lightCycleTime">
                            <div>Start</div>
                            <Select
                                style={{ 
                                    fontFamily: 'inherit',
                                    fontSize: '15px',
                                    color: '#000000'
                                }}
                                value={startingTime}
                                name="newStartingTime"
                                onChange={handleLightCycleTimeChange}
                                >
                                <MenuItem value={'0:00'}>12am</MenuItem>
                                <MenuItem value={'1:00'}>1am</MenuItem>
                                <MenuItem value={'2:00'}>2am</MenuItem>
                                <MenuItem value={'3:00'}>3am</MenuItem>
                                <MenuItem value={'4:00'}>4am</MenuItem>
                                <MenuItem value={'5:00'}>5am</MenuItem>
                                <MenuItem value={'6:00'}>6am</MenuItem>
                                <MenuItem value={'7:00'}>7am</MenuItem>
                                <MenuItem value={'8:00'}>8am</MenuItem>
                                <MenuItem value={'9:00'}>9am</MenuItem>
                                <MenuItem value={'10:00'}>10am</MenuItem>
                                <MenuItem value={'11:00'}>11am</MenuItem>
                                <MenuItem value={'12:00'}>12pm</MenuItem>
                                <MenuItem value={'13:00'}>1pm</MenuItem>
                                <MenuItem value={'14:00'}>2pm</MenuItem>
                                <MenuItem value={'15:00'}>3pm</MenuItem>
                                <MenuItem value={'16:00'}>4pm</MenuItem>
                                <MenuItem value={'17:00'}>5pm</MenuItem>
                                <MenuItem value={'18:00'}>6pm</MenuItem>
                                <MenuItem value={'19:00'}>7pm</MenuItem>
                                <MenuItem value={'20:00'}>8pm</MenuItem>
                                <MenuItem value={'21:00'}>9pm</MenuItem>
                                <MenuItem value={'22:00'}>10pm</MenuItem>
                                <MenuItem value={'23:00'}>11pm</MenuItem>
                            </Select>
                        </div>

                        <div className='lightCycleTime'>
                            <div>Stop</div>
                            <Select
                                value={stoppingTime}
                                name="newStoppingTime"
                                onChange={handleLightCycleTimeChange}
                                style={{ 
                                    fontFamily: 'inherit',
                                    fontSize: '15px',
                                    color: '#000000'
                                }}
                                >
                                <MenuItem value={'0:00'}>12am</MenuItem>
                                <MenuItem value={'1:00'}>1am</MenuItem>
                                <MenuItem value={'2:00'}>2am</MenuItem>
                                <MenuItem value={'3:00'}>3am</MenuItem>
                                <MenuItem value={'4:00'}>4am</MenuItem>
                                <MenuItem value={'5:00'}>5am</MenuItem>
                                <MenuItem value={'6:00'}>6am</MenuItem>
                                <MenuItem value={'7:00'}>7am</MenuItem>
                                <MenuItem value={'8:00'}>8am</MenuItem>
                                <MenuItem value={'9:00'}>9am</MenuItem>
                                <MenuItem value={'10:00'}>10am</MenuItem>
                                <MenuItem value={'11:00'}>11am</MenuItem>
                                <MenuItem value={'12:00'}>12pm</MenuItem>
                                <MenuItem value={'13:00'}>1pm</MenuItem>
                                <MenuItem value={'14:00'}>2pm</MenuItem>
                                <MenuItem value={'15:00'}>3pm</MenuItem>
                                <MenuItem value={'16:00'}>4pm</MenuItem>
                                <MenuItem value={'17:00'}>5pm</MenuItem>
                                <MenuItem value={'18:00'}>6pm</MenuItem>
                                <MenuItem value={'19:00'}>7pm</MenuItem>
                                <MenuItem value={'20:00'}>8pm</MenuItem>
                                <MenuItem value={'21:00'}>9pm</MenuItem>
                                <MenuItem value={'22:00'}>10pm</MenuItem>
                                <MenuItem value={'23:00'}>11pm</MenuItem>
                            </Select>
                        </div>
                    </React.Fragment>
                    : null
                }
            </div>  
            
        </div>
    )
}

//Default props
Lighting.defaultProps = {
    lightState: false,
    lightCycleState: false,
    startingTime: '0:00',
    stoppingTime: '0:00'
}
