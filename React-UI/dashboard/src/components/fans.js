import React from 'react'
import './css/lighting.css'
import Switch from '@material-ui/core/Switch'

export const Fans = (props) => {
    //Light settings props
    const { fanState, handleFanChange } = props

    return (
        <div className="lightingContainer">
            <h1 className="titleText">
                <img src='./fan.png' alt='fan icon' className='iconImg' />
                Fan
            </h1>            
            
            <div className="lightSetting">
                <span>Fan</span>
                <Switch 
                    checked={fanState} 
                    value="Fanboi"
                    onChange={handleFanChange}
                />
            </div>
            
        </div>
    )
}